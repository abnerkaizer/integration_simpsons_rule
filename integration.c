#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <math.h>
#include <time.h>

/*
Regra de Simpson para o calculo aproximado de uma integral definida em [a,b]
com b>a.
https://pt.wikipedia.org/wiki/F%C3%B3rmula_de_Simpson
*/

//constantes
#define N 8 //tem que ser par.
#define A 1
#define B 13
#define H 1.5//(B-A)/N
#define SIZE 7//N-1
//variaveis globais
pthread_mutex_t mutex;

clock_t start, end;

double cpu_time_used;

double x[SIZE];

double result = 0;
//registrador contendo as informações necessarias pelas threads
typedef struct thread
{
	int id;
	double x;
}Thread;

void *thread(void *arg);

double func(double x){
	return log(x);//f(x) = ln x
}

int main()
{
	pthread_t tid[N-1];
	int i;
	start = clock();
	//calcula os valores de x que serão necessarios depois.
	for (i = 0; i <SIZE; ++i)
	{
		x[i] = A + (i+1)*H;
		//printf("%d: %lf\n",i ,x[i]);
	}
	Thread *arg;

	pthread_mutex_init(&mutex, NULL);

	result = func(A)+func(B);//o coeficiente do primeiro e ultimo elementos é 1.

	//inicia as threads
	for(i=0;i<SIZE;i++){
		arg = (Thread*) malloc(sizeof(Thread));
		if (arg==NULL)
		{
			printf("ERRO: malloc()\n");
			return -1;
		}
		arg->id = i+1;
		arg->x = x[i];
		
		if (pthread_create(&tid[i], NULL, thread, (void*) arg)) {
			printf("--ERRO: pthread_create()\n"); 
			return -1;
		}
	}
	for (i=0; i<SIZE; i++) {
		if (pthread_join(tid[i], NULL)) {
			printf("--ERRO: pthread_join() \n"); 
			return -1; 
		} 
	}

	result = result*(H/3);

	end = clock();

	printf("Resultado multithread: %lf\n", result);

	cpu_time_used = ((double)(end - start))/ CLOCKS_PER_SEC;
	printf("%lfs\n", cpu_time_used);

	pthread_mutex_destroy(&mutex);
	pthread_exit(NULL);
    return 1;
}

void *thread(void *arg){
	Thread *args = (Thread*) arg;
	int id  = args->id;
	double aux = args->x;
	pthread_mutex_lock(&mutex);
	if(id%2!=0) {//se id for impar o coeficiente é 4.
		result += 4*func(aux);
		//printf("Thread %d, valor x %lf e valor func: %lf\n", id, aux,func(aux));
	}
	else {
		result += 2*func(aux);
		//printf("Thread %d, valor x %lf e valor func: %lf\n", id, aux,func(aux));
	}
	pthread_mutex_unlock(&mutex);
	free(arg);

	pthread_exit(NULL);
	return 0;
}
