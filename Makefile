#!/usr/bin/zsh

CC=gcc
CFLAGS= -Og -g -Wall -Wextra -mtune=native
LIBS= -lm -pthread
TARGET1= integration
TARGET2= sequencial
all:
	$(CC) $(CFLAGS) $(LIBS) integration.c -o $(TARGET1)
	$(CC) $(CFLAGS) $(LIBS) integration_seq.c -o $(TARGET2)
int:
	$(CC) $(CFLAGS) $(LIBS) integration.c -o $(TARGET1)
seq:
	$(CC) $(CFLAGS) $(LIBS) integration_seq.c -o $(TARGET2)
clean:
	rm $(TARGET1)
	rm $(TARGET2)