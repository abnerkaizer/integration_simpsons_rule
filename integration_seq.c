#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

/*
Regra de Simpson para o calculo aproximado de uma integral definida em [a,b]
com b>a.
https://pt.wikipedia.org/wiki/F%C3%B3rmula_de_Simpson
*/

#define N 8 //tem que ser par.
#define A 1
#define B 13
#define H 1.5//(B-A)/N

clock_t start, end;
double cpu_time_used;

double func(double x){
	return log(x);//f(x) = ln x
}
int main()
{
	double result = func(A) + func(B);
	double x[N+1];
	start = clock();
	x[0] = func(A);
	x[N] = func(B);
	result = x[0]+x[N];
	for (int i = 1; i <N; ++i)
	{
		x[i] = A + i*H;
	}

	for (int i = 1; i <N; ++i)
	{
		if(i%2!=0) {
			result += 4*func(x[i]);
		}
		else {
			result += 2*func(x[i]);
		}
	}
	result = result*(H/3);
	end = clock();
	printf("Resultado sequencial:%lf\n", result);
	cpu_time_used = ((double)(end - start))/ CLOCKS_PER_SEC;
	printf("%lfs\n", cpu_time_used);
	return 0;
}